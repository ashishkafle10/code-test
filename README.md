# ABC Motor Developer Test
This project is developed by Ashish Kafle for Developer Coding Test. It is a single page application with the features of lising the vehicles, booking the vehicles either for loan or testdrive and update the vehicles using the *Repository Design Pattern*.

## Server Requirements
-  Larvel 6.x
- Php >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- Apache or Nginx
- Mysql
- Composer
- npm 

## How to install and run
1.  Clone the repository
2.  Run *php artisan key:generate*
3. Run *composer update*
4. Run *npm install* 
5. Run *npm install vue-router vue-axios --save*  - to install vue-router and axios
6. Copy .env.example to .env file
7. Set Database Name and Password 
8. Run *php artisan migrate* to create table in the Database selected @ step 7
9.  Set the base url of the project in ***APP_URL*** and ***MIX_APP_URL*** ,  ***MIX_APP_URL*** is used by vue components
10. To run unit testing , use  command "./vendor/bin/phpununit" or "/vendor/bin/phpunit" or look into laravel testing documentation [Laravel Testing](https://laravel.com/docs/6.x/testing "Laravel Testing")
11. To run project use *npm run watch*

## App Flow
1. First App will land into home page which will call *GET vehicles*  to list all the vehicles available. If one of the vehicle are currently booked the status of the vehicle will be "Checked Out" or "Available" in case of vehicle is not booked or already returned.
2.  For Status "Checked Out" Vehicle, *Edit * button will enable and on click page will load to allow user to update the information. It will first calls *GET orders/{id}* to prefill the existing value and *PUT order/{id}* to update the order.
3. In case of status is "Available",  *Book* button will be enable and on click a page will load which will allows to create new booking for vehicle and calls *POST order* api to create the bookings of vehicle
4. After step 2 or 3, app will go to step 1. 
