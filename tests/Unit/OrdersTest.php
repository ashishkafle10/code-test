<?php


class OrdersTest extends \Tests\TestCase
{
    /**
     * function to return specific booked order
     */
    public function testGetOrders(){
        $this->createOrder();
        $this->get('api/order/440')->assertJsonStructure([
            'status',
            'payload' =>['name','checkout_timestamp','type','return_timestamp','condition','make_id']
        ])->assertStatus(200);
    }

    /**
     * Function to test successful booking
     */
    public function testBookOrders(){
        $request = [
            "make_id" =>441,
            "name"=>"Ashish Kafle",
            "type"=>"0",
            "checkout_timestamp"=>"2020-11-22T21:12"];
        $this->post('api/order',$request)->assertJsonStructure([
            'status',
            'message'
        ])->assertStatus(200);
    }

    /**
     * Function to test successful booking
     */
    public function testBookOrdersValidationErrors(){
        $data = [
            ];
        $this->post('api/order',$data)->assertJsonStructure([
            'status',
            'message' => [
                'name',
                'make_id',
                'type',
                'checkout_timestamp'



            ]
        ])->assertStatus(422);
    }

    /**
     * Function to test updating the booking
     */
    public function testUpdateBooking(){
        $this->createOrder();
       $request = array (
            'name' => 'Ashish Kafle',
            'type' => 0,
            'return_timestamp' => '2020-11-22T21:18',
            'condition' => 'good',
            'make_id' => 440,
            'checkout_timestamp' => '2020-11-22T21:11',
            'created_at' => '2020-11-22 09:25:51',
            'updated_at' => '2020-11-22 09:33:50',
        );
        $this->put('api/order/440',$request)->assertJsonStructure([
            'status',
            'message'
        ])->assertStatus(200);

    }

    /**
     * Function to test validation error  updating booking
     */
    public function testUpdateBookingValidationErrors(){
        $data = [
        ];
        $this->createOrder();
        $this->put('api/order/440',$data)->assertJsonStructure([
            'status',
            'message' => ['name','checkout_timestamp','type','return_timestamp','condition','make_id']
        ])->assertStatus(422);
    }




}
