<?php


class VehicleTest extends \Tests\TestCase
{
    /**
     * function to test if vehicles are returned or not
     */
    public function testGetVehicle(){
     $this->get('api/vehicles')->assertJsonStructure([
         '*' => [
             'MakeId',
             'MakeName',
             'VehicleTypeId',
             'VehicleTypeName',
             'status'
         ]
     ])->assertStatus(200);
    }

}
