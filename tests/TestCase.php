<?php

namespace Tests;

use App\Orders;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate');
    }

    public function createOrder(){
        Orders::create([
            'name' => "Tester 1234",
            'make_id' => 440,
            'type' => 1,
            'checkout_timestamp' => '2020-11-22T20:49'
        ]);
    }
}
