<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = "order";

    protected $fillable = ['name','checkout_timestamp','type','return_timestamp','condition','make_id'];

    protected $attributes = [
        'return_timestamp' => null,
        'condition' => '',

        'type' => 0 //0 means testdrive, 1 means Loan
    ];

    public function getCheckoutTimestampAttribute($value){
        if($value === null) return $value;
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function SetCheckoutTimestampAttribute($value){
        $this->attributes["checkout_timestamp"] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function getReturnTimestampAttribute($value){
        if($value === null) return $value;
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }

    public function SetReturnTimestampAttribute($value){
        $this->attributes["return_timestamp"] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }


}
