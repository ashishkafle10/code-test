<?php


namespace App\Repo;


interface OrderInterface
{
    /**
     * Function to get specific vehicle order
     * @param $make_id
     * @return mixed
     */
    public function getSpecificOrder($make_id);

    /**
     * Function to return all the records where returned date is null
     * @return mixed
     */
    public function getAllRecord();

    /**
     * Function to create new booking of vehicle
     * @param array $request
     * @return mixed
     */
    public function createOrder(array $request);

    /**
     * Function to upate booked vehicle
     * @param $make_id
     * @param array $request
     * @return mixed
     */
    public function updateOrder($make_id,array $request);

}
