<?php


namespace App\Repo;


use App\Orders;

class OrderRepo implements OrderInterface
{
    private $order;
    public function __construct(Orders $order)
    {
        $this->order = $order;
    }

    /**
     * Function to get specific vehicle order
     * @param $make_id
     * @return mixed
     */
    public function getSpecificOrder($make_id)
    {
       return $this->order->where('make_id',$make_id)->whereNull('return_timestamp')->firstOrFail();
    }

    /**
     * Fetch non returned booked vehicles order record
     * @return mixed
     */
    public function getAllRecord()
    {
        return $this->order->whereNull('return_timestamp')->get();
    }


    /**
     * Function to create new booking of vehicle
     * @param array $request
     * @return mixed
     */
    public function createOrder(array $request)
    {
        return $this->order->create($request);
    }

    /**
     * Function to upate booked vehicle
     * @param $make_id
     * @param array $request
     * @return mixed
     */
    public function updateOrder($make_id,array $request)
    {
       $order = $this->getSpecificOrder($make_id);
       return $order->update($request);
    }


}
