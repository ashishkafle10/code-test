<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Repo\OrderInterface;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;

class OrderController extends Controller
{
    private $order;
    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }


    /**
     * Function to return specific booked order based on @param $make_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecific($id){
        try{
            /**
             * fetch specific booked vehicle info where the car is not returned yet, tnrows exception on errror
             */
            $order = $this->order->getSpecificOrder($id);

            return response()->json([
                'status'=> 200,
                'payload' => $order
            ]);
        }
        catch (\Exception $ex){
            Log::error('error-fetching-order',[
                [
                    'status'=>500,
                    'message' => "Error Fetching record"
                ]
            ]);
            return response()->json([
                'status'=>500,
                'message' => "Error Fetching record"
            ],500);
        }
    }

    /**
     * Function to book new Vehicle
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrder(Request  $request){

        try{
            $request->validate([
                'name' => "required|string",
                "make_id" => "required|int",
                "checkout_timestamp" => "required|date_format:Y-m-d\TH:i",
                'type' => 'required|boolean'
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 422,
                'message' => $ex->errors()
            ],422);
        }

        try{
            /**
             * create new booking, if fails throws exception
             */
             $this->order->createOrder($request->all());


            return response()->json([
                'status' => 200,
                'message' => 'Vehicle Booked Successfully'
            ]);
        }
        catch (QueryException $ex){
            Log::error('error-creating-order',[
                [
                    'status'=>500,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error in Booking'
            ],500);
        }
        catch (\Exception $ex){
            Log::error('error-creating-order',[
                [
                    'status'=>500,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error in Booking'
            ],500);
        }


    }

    /**
     * Function to update booked vehicle
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOrder($id, Request  $request){

        try{
            $request->validate([
                'name' => "required|string",
                "make_id" => "required|int",
                "checkout_timestamp" => "required|date_format:Y-m-d\TH:i",
                'type' => 'required|boolean',
                'return_timestamp' => 'required|after:checkout_timestamp|date_format:Y-m-d\TH:i',
                'condition' => 'required|string'
            ]);
        }
        catch (ValidationException $ex){
            return response()->json([
                'status' => 422,
                'message' => $ex->errors()
            ],422);
        }

        try{
           $this->order->updateOrder($id,$request->all());
            return response()->json([
                'status' => 200,
                'message' => 'Information updated successfully'
            ],200);
        }
        catch (QueryException $ex){
            Log::error('error-updating-order',[
                [
                    'status'=>500,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error in updating order'
            ],500);
        }
        catch (\Exception $ex){
            Log::error('error-updating-order',[
                [
                    'status'=>500,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'status' => '500',
                'message' => 'Error in updating order'
            ],500);
        }


    }

}
