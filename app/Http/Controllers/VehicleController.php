<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Repo\OrderInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class VehicleController extends Controller
{
    private $order;
    /**
     * VehicleController constructor.
     */
    public function __construct(OrderInterface  $order)
    {
        $this->order = $order;
    }


    /**
     * Calls the api using guzzle and returns the vehicle info
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAvailableVehicles(){

        try{
            /**
             * call the url to get available vehicles
             */
            $vehicle = file_get_contents("https://vpic.nhtsa.dot.gov/api/vehicles/GetMakesForVehicleType/car?format=json");
            /**
             * decode the response string to array
             */
            $vehicles = json_decode($vehicle,true);
            /**
             * fetch all the booked vehicles
             */
            $orders = $this->order->getAllRecord();
            /**
             * take the Results index only
             */
            $vehicles = $vehicles['Results'];
            /**
             * Loop through each $vehicles and compare with booked vehicles to set the status
             */
            foreach ($vehicles as $key =>$vehicle){
                /**
                 * default status is 0 which means not booked
                 */
                $vehicles[$key]['status'] = 0;
                /**
                 * loop through each booked vehicles
                 */
                foreach ($orders as $order){
                    /**
                     * if booked vehicle id is equals to fetched available vehicle id and is not returned then set status to 1 which means vehicle is unavailable
                     */
                    if($order['make_id'] == $vehicle['MakeId'] && $order['return_timestamp'] == null  ){

                        $vehicles[$key]['status'] = 1;
                    }


                }
            }
            /**
             * Sort By Make Name in ASC order
             */
            $sorted_vehicles = collect($vehicles)->sortBy( 'MakeName')->values()->all();



            return response()->json($sorted_vehicles);
        }
        catch ( \Exception $ex){
            Log::error('error-fetching-vehicles',[
                [
                    'status'=> 500,
                    'message' => $ex->getMessage()
                ]
            ]);
            return response()->json([
                'message' => "Error fetching vehicle record",
                'status' => 500
            ],500);
        }

    }

}
