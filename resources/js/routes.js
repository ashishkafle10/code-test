import AllVehicles from './components/AllVehicles.vue';
import BookVehicle from "./components/BookVehicle";
import EditVehicle from "./components/EditVehicle";
export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllVehicles
    },
    {
        name: 'book',
        path: '/book',
        component: BookVehicle,
        props(route) {
            return {
                make_name: route.params.make_name,
                id : route.params.id
            }
        }
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditVehicle,
        props(route) {
            return {  make_name: route.params.make_name }
        }
    }
];
